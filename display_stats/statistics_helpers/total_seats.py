
from django.db.models import Max, Sum
from ..models import CourseSpring2017, SectionSpring2017, EnrollmentSpring2017


def get_total_seats_by_subject_specific_date(pub_time, term_models, term):
    """returns a dictionary {<course>:<open seats for the given time>}
        Need to be carefully with the supplied pub_time, it should match
        EXACTLY the point in time form the DB"""
    Enrollment =  term_models[term]['enrollment_model']
    Course = term_models[term]['course_model']

    subject_enrollments = dict()

    all_courses = Course.distinct_subjects()

    for course in all_courses:
        enrollment = Enrollment.objects.filter(
                class_number__course__course_subject=course,
                publication_time=pub_time).aggregate(Sum('open_seats'))

        if enrollment['open_seats__sum']:
            subject_enrollments[course] = enrollment['open_seats__sum']
        else:
            subject_enrollments[course] = 0
    return subject_enrollments

def get_total_seats_by_subjects(term_models, term):
    '''
    Sums the enrollment cap for all sections for a certain subject
    Does it for all subjects
    Returns sorted by subject alphabetically
    '''
    Course = term_models[term]['course_model']
    Section = term_models[term]['section_model']

    all_courses = Course.distinct_subjects()
    total_enrollment_for_all_courses = []
    for course in all_courses:

        subject_seats = Section.objects.filter(course__course_subject=course)\
                            .aggregate(Sum('section_max_enrollment'))

        # If the course never had a section then the enrollment would be NONE
        if subject_seats['section_max_enrollment__sum']:
            total_enrollment_for_all_courses.append((course, subject_seats['section_max_enrollment__sum']))

    return sorted(total_enrollment_for_all_courses, key=lambda x: x[1], reverse=True)

