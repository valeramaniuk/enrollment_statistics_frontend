import operator

import plotly.plotly as py
import plotly.graph_objs as go
import datetime as dt
from collections import OrderedDict

from django.db.models import Max, Sum
from django.core.exceptions import ObjectDoesNotExist
from dynamo import cache
from dynamo.credentials import get_plotly_credentials
from display_stats.statistics_helpers import total_seats
from ..models import SectionSpring2017, EnrollmentSpring2017, CourseSpring2017, SectionScheduleSpring2017
from .logger import logger

_TIME_CUT_OFF = ('May 1 2017', '%b %d %Y')


def add_dates_with_zero_enrollment(enrollment_dict):
    '''
    accepts a dictionary {<date>:<open_seats>}
    and adds missing dates by assigning <open_seats> to ZERO
    '''

    # The last date we are interested in
    time_cutoff = dt.datetime.strptime(_TIME_CUT_OFF[0], _TIME_CUT_OFF[1])
    # if time_cutoff is not passed then we are interested in everything
    publication_times = EnrollmentSpring2017.all_pub_times()

    for pub_time in publication_times:
        if pub_time not in enrollment_dict:
            enrollment_dict[pub_time] = 0

    return enrollment_dict


def draw_simple_open_seats_graph(x_data, y_data, name, type=None):
    '''
    Accepts:
            x_data (presumably time)
            y_data (presumably open seats)
            name - the filename of the graph
            retuns full url suitable for embedding
    '''
    assert isinstance(x_data[0], dt.datetime), "Graph drawing module accepts time as a list datetime.datetime ONLY"
    x_data = [x.strftime('%b-%d %I%p') for x in x_data]

    if type == 'course':
        fillcolor = 'rgb(31, 119, 180)',
        line = dict(
            color='rgb(148, 103, 189)',
            width=1)
    else:
        fillcolor = 'rgb(100, 149, 237)',
        line = dict(
            color='rgb(100, 149, 237)',
            width=1)

    trace1 = go.Scatter(
        x=x_data,
        y=y_data,
        fill='tonexty',
        mode='lines',
        line=line,
        fillcolor=fillcolor
    )

    data = [trace1, ]
    layout = go.Layout(
        autosize=True,
        margin=go.Margin(
            l=30,
            r=30,
            b=80,
            t=30,
            pad=0
        ),
        legend=dict(x=.6, y=0.7)
    )

    fig = go.Figure(data=data, layout=layout)
    url = py.plot(fig, filename=name, auto_open=False)

    embed = url.replace('https:', '') + '.embed'

    return embed


def draw_detailed_section_graph(list_of_sections):
    '''accepts a list of <Section objects>
        and returns a list of plotly urls'''

    CONF = get_plotly_credentials()
    py.sign_in(username=CONF['username'], api_key=CONF['api_key'])

    list_of_embeds = []

    for section in list_of_sections:
        ordered_dict = OrderedDict()
        is_cache = cache.check_if_cache_exists(str(section.class_number) + "_graph")
        if is_cache:
            list_of_embeds.append(is_cache)
        else:

            # filename constructor
            name = 'csun/sections/section_open_seats_' + str(section.class_number)

            # get all enrollments data for this section
            enrollments_for_this_section = EnrollmentSpring2017.objects.filter(
                class_number=section.class_number,
            ).order_by('publication_time')

            # extract dates and corresponding open seats data
            for enrollment in enrollments_for_this_section:
                ordered_dict[enrollment.publication_time] = enrollment.open_seats
            ordered_dict = add_dates_with_zero_enrollment(ordered_dict)

            # gets a list of tuples (<datetime>, <open seats>) a sorted be <datetime>
            ordered_dict = OrderedDict(sorted(ordered_dict.items()))
            x_data = [x for x, y in ordered_dict.items()]
            y_data = [y for x, y in ordered_dict.items()]

            embed = draw_simple_open_seats_graph(x_data, y_data, name)
            list_of_embeds.append(embed)

            # cache the graph
            cache.create_cache(str(section.class_number) + "_graph", embed)

    return list_of_embeds


def draw_detailed_course_graph(subject, level=None):
    """
        Draws the seats vs time graph for a specific course
        of for the subject in general(if level wasn't specified)
    """

    CONF = get_plotly_credentials()
    py.sign_in(username=CONF['username'], api_key=CONF['api_key'])

    x_data = []
    y_data = []
    # Initialize so IDE won't whine, probably unnecessary
    filename = 'course'

    if subject and level:
        cache_meta = "{}_{}_graph".format(subject, level)
    else:
        cache_meta = "{}_graph".format(subject)

    is_cache = cache.check_if_cache_exists(cache_meta)

    if is_cache:
        return is_cache

    else:
        for pub_time in EnrollmentSpring2017.all_pub_times():
            # for every single point in time that we have any information for
            # even if this specific course doesn't have any data at this point

            # if level is specified - get all enrollment objects
            # for this course for this date
            if level:
                subject_open_seat_list = EnrollmentSpring2017.objects.filter(
                    class_number__course__course_subject=subject,
                    class_number__course__course_level=level,
                    publication_time=pub_time)
                filename = "{}_{}".format(subject.replace(' ', '_'), level)

            # if no level is specified get get all enrollment objects
            # just for the subject for this date
            else:
                subject_open_seat_list = EnrollmentSpring2017.objects.filter(
                    class_number__course__course_subject=subject,
                    publication_time=pub_time)
                filename = "{}".format(subject.replace(' ', '_'))

            # if there is some data for this point in time - sum the number of open seats
            # for the given course (it may have several sections)
            # append th sum to the list
            if subject_open_seat_list:
                y_data.append(subject_open_seat_list.aggregate(Sum('open_seats'))['open_seats__sum'])
            else:
                # but is there is no data for this point in time - assume zero
                # number of seats
                y_data.append(0)

            x_data.append(pub_time)

        url = draw_simple_open_seats_graph(x_data, y_data, 'csun/courses/' + filename, type='course')
        cache.create_cache(cache_meta, url)
        return url


def draw_open_seats_all_subjects(term_models, term):
    '''
        Shows cumulative open seats for all Math, STEM and NonStem sections
        First gets the list of STEM sections, then the list of MATH sections
        and then Non-STEM = ALL - MATH - STEM
    '''
    CONF = get_plotly_credentials()
    py.sign_in(username=CONF['username'], api_key=CONF['api_key'])

    # The list is arbitrary, I composed it myself
    STEM_LIST = ['SCI', 'GEOL', 'CM', 'EOH', 'IS', 'PHYS', 'PHY', 'ECE', 'ASTR', 'URBS', 'CADV',
                 'COMP', 'A M', 'M E', 'CHEM', 'MSE', 'GEOG', 'CE', 'SOM', 'BIOL', 'PHSC', 'HSCI', ]

    # The last date we are interested in
    time_cutoff = dt.datetime.strptime(_TIME_CUT_OFF[0], _TIME_CUT_OFF[1])

    # Choose the models according to the value of the <term>
    # From example SectionSpring2017 for Spring and SectionFall2017
    # for the Fall

    Section=term_models[term]['section_model']
    Enrollment =  term_models[term]['enrollment_model']

    x_data = []
    y_1_data = []
    y_2_data = []
    y_3_data = []
    sum_stem_enrollments = dict()
    sum_non_stem_enrollments = dict()
    sum_math_enrollments = dict()

    # List of STEM sections
    all_stem_sections = Section.objects.filter(course__course_subject__in=STEM_LIST). \
        values_list('class_number', flat=True)

    # List of ALL sections minus STEM sections minus MATH sections:
    #
    # At first its just the a of ALL sections
    # but later we exclude one course at the time
    all_non_stem_sections = Section.objects.all()

    # The query is lazy so I hope it will make one giant query
    # rather than many small ones
    for course_title in STEM_LIST:
        all_non_stem_sections = all_non_stem_sections.exclude(course__course_subject=course_title)

    # Excluding MATH from non STEM
    all_non_stem_sections = all_non_stem_sections.exclude(course__course_subject='MATH')
    # Make a list of nonSTEM nonMATH class_numbers
    all_non_stem_sections = all_non_stem_sections.values_list('class_number', flat=True)

    # All math sections
    all_math_sections = Section.objects.filter(course__course_subject='MATH')
    all_math_sections = all_math_sections.values_list('class_number', flat=True)

    # The list of all points in time for which we got enrollment data
    all_publication_times = Enrollment.all_pub_times()

    # TODO:  this may be factored, we do the same operation 3 times
    for pub_time in all_publication_times:
        stem_enrollments = Enrollment.objects.filter(
            class_number__in=all_stem_sections,
            publication_time=pub_time)
        if not stem_enrollments:
            sum_stem_enrollments['open_seats__sum'] = 0
        else:
            sum_stem_enrollments = stem_enrollments.aggregate(Sum('open_seats'))

        non_stem_enrollments = Enrollment.objects.filter(
            class_number__in=all_non_stem_sections,
            publication_time=pub_time)
        if not non_stem_enrollments:
            sum_non_stem_enrollments['open_seats__sum'] = 0
        else:
            sum_non_stem_enrollments = non_stem_enrollments.aggregate(Sum('open_seats'))

        math_enrollments = Enrollment.objects.filter(
            class_number__in=all_math_sections,
            publication_time=pub_time)
        if not math_enrollments:
            sum_math_enrollments['open_seats__sum'] = 0
        else:
            sum_math_enrollments = math_enrollments.aggregate(Sum('open_seats'))

        x_data.append(pub_time.strftime('%b-%d %I%p'))

        y_1_data.append(sum_stem_enrollments['open_seats__sum'])
        y_2_data.append(sum_non_stem_enrollments['open_seats__sum'])
        y_3_data.append(sum_math_enrollments['open_seats__sum'])

    # The graph is stacked so the second value should be added to the 1st one to "appear"
    # stacked on top
    y1_stck = y_1_data
    y2_stck = [y0 + y1 for y0, y1 in zip(y_1_data, y_2_data)]
    y3_stck = [y0 + y1 + y2 for y0, y1, y2 in zip(y_1_data, y_2_data, y_3_data)]

    # We also pass the actual "non-stacked" data to each trace
    # so it can be displayed
    # The very top trace will also display the sum
    #
    trace3_text = []
    for tr3, stacked_total in zip(y_3_data, y3_stck):
        trace3_text.append("MATH {} seats, <br> total is {} seats".format(tr3, stacked_total))

    trace1_text = ["STEM: {} seats".format(tr1) for tr1 in y_1_data]
    trace2_text = ["nonSTEM: {} seats".format(tr2) for tr2 in y_2_data]

    trace1 = go.Scatter(x=x_data,
                        y=y1_stck,
                        mode='lines',
                        fill='tonexty',
                        name='STEM',
                        text=trace1_text,
                        hoverinfo='text+x')

    trace2 = go.Scatter(x=x_data,
                        y=y2_stck,
                        fill='tonexty',
                        mode='lines',
                        name='nonSTEM',
                        text=trace2_text,
                        hoverinfo='text+x')

    trace3 = go.Scatter(x=x_data,
                        y=y3_stck,
                        fill='tonexty',
                        mode='lines',
                        name='MATH',
                        text=trace3_text,
                        hoverinfo='text+x')

    data = [trace1, trace2, trace3]

    layout = go.Layout(
        autosize=False,
        margin=go.Margin(
            l=30,
            r=30,
            b=50,
            t=30,
            pad=0
        ),
        legend=dict(x=.6, y=0.7)
    )

    fig = go.Figure(data=data, layout=layout)
    filename = 'csun/overview/total_vs_stem_enrollment_'+str(term)
    url = py.plot(fig, filename=filename, auto_open=False)
    embed = url.replace('https:', '') + '.embed'
    return embed


def draw_wasted_seats_by_section(term_models, term):
    # TODO : it's should be done through select_related, now we have multiple db hits

    # what is below the threshold won't make to the graph
    _PERCENTAGE_THRESHOLD = 10

    # login into plotly, credentials come from the external config table
    CONF = get_plotly_credentials()
    py.sign_in(username=CONF['username'], api_key=CONF['api_key'])

    Section = term_models[term]['section_model']
    Enrollment = term_models[term]['enrollment_model']
    latest_date = Enrollment.latest_pub_date()

    x_y_text = []
    y_data = []
    x_data = []
    text = []
    color_list = []

    all_sections = Section.objects.all().select_related()
    for section in all_sections:

        max_enrollment = section.section_max_enrollment
        # there are maybe many schedules
        # we can use any one
        instructor = section.section_schedule.all()[0].instructor
        course_subject = section.course.course_subject
        course_level = section.course.course_level

        # enrollment at the date specified by the variable latest_date
        try:
            final_open_seats = Enrollment.objects.get(publication_time=latest_date,
                                                      class_number=section.class_number).open_seats
        except ObjectDoesNotExist:
            # We expect many of them to not exist
            # maybe it's not the best use of exceptions
            # but is less cumbersome then .filter in this case
            # If there is no info on enrollment for this date - assume that it's full
            # it also can be cancelled by there is no way to know
            #
            final_open_seats = 0

        # Percentage of wasted seats, rounded
        wasted_percentage = int(100 * final_open_seats / max_enrollment)

        # Showing only sections with wasted seats above some percentage
        # or below zero which represents overbooking
        if wasted_percentage > _PERCENTAGE_THRESHOLD or wasted_percentage < 0:
            # list of triples (<section #>, <% of wasted>, <detailed text>)
            # have to add a symbol before the section name, otherwise it treats it like number
            x_y_text.append(('section {}'.format(section.class_number),
                             wasted_percentage,
                             "section {}, {}-{}, {}".format(section.class_number,
                                                            course_subject,
                                                            course_level,
                                                            instructor)))

    # it's a list of 3-tuples ((x,y,text),(x1,y1,text1),..)
    # we sort those based on the value of the second element (value of y)
    x_y_text.sort(key=operator.itemgetter(1), reverse=True)

    for triple in x_y_text:
        x_data.append(triple[0])
        y_data.append(triple[1])
        text.append(triple[2])

        # if the value is above the threshold then it's color#1
        # if its below zero then its another color
        # below zero is impossible in this model however
        if triple[1] < 0:
            color_list.append('rgba(204,204,204,0.8)')
        else:
            color_list.append('rgba(222,45,38,0.8')

    # Drawing the graph:

    trace1 = go.Bar(x=x_data,
                    y=y_data,
                    text=text,
                    marker=dict(
                        color=color_list
                    )
                    )

    data = [trace1, ]
    layout = go.Layout(
        title='Percentage of vacant seats by {}  by sections'.format(latest_date),
        margin=go.Margin(
            l=30,
            r=80,
            b=80,
            t=30,
            pad=0
        ),
        legend=dict(x=.6, y=0.7)
    )
    fig = go.Figure(data=data, layout=layout)
    url = py.plot(fig, filename='csun/overview/wasted_seats_by_section_'+str(term), auto_open=False)
    return url


def draw_seats_by_subject_overview(term_models, term):
    CONF = get_plotly_credentials()
    py.sign_in(username=CONF['username'], api_key=CONF['api_key'])

    total_enrollment_for_all_courses = total_seats.get_total_seats_by_subjects(term_models, term)

    # CAREFUL! pub_date should match the datetime form the DB EXACTLY
    # for that we take the very last date available for the semester
    cut_off_time = term_models[term]['enrollment_model'].latest_pub_date()

    # pub_date = dt.datetime.strptime(cut_off_time, '%Y-%m-%d %H:%M:%S')
    total_open_seats_by_date = total_seats.get_total_seats_by_subject_specific_date(cut_off_time,
                                                                                    term_models=term_models,
                                                                                    term=term)

    x = [a[0] for a in total_enrollment_for_all_courses]
    # open seats total, at the beginning
    y_1 = [a[1] for a in total_enrollment_for_all_courses]
    # open seats by date
    y_2 = []

    for course in x:
        y_2.append(total_open_seats_by_date[course])

    # substract y_2 from y_1 by element
    # since they will be stacked
    y_1 = [i - j for i, j in zip(y_1, y_2)]

    trace1 = go.Bar(x=x, y=y_1, name='taken')
    trace2 = go.Bar(x=x, y=y_2, name='open {}'.format(cut_off_time.strftime('%b %d %Y')))

    data = [trace1, trace2]
    layout = go.Layout(
        barmode='stack',
        autosize=False,
        margin=go.Margin(
            l=30,
            r=80,
            b=80,
            t=30,
            pad=0
        ),
        legend=dict(x=.6, y=0.7)
    )
    fig = go.Figure(data=data, layout=layout)
    filename = 'csun/overview/max_seats_by_subject-bar_'+str(term)
    url = py.plot(fig, filename=filename, auto_open=False)
    return url


def draw_instructors_units(term_models, term):

    Section = term_models[term]['section_model']

    instructors_units = dict()
    x_data = []
    y_data = []
    color_list = []

    all_sections = Section.objects.all().select_related()
    for section in all_sections:
        units = section.course.course_units
        instructor = section.section_schedule.all()[0].instructor
        if instructor in instructors_units:
            instructors_units[instructor] += units
        else:
            instructors_units[instructor] = units





    # remove 'Staff' from appearing in the graph
    # but save the value to present
    if 'Staff' in instructors_units:
        staff_units = instructors_units['Staff']
        del instructors_units['Staff']
    else:
        staff_units = None

    # sort the dictionary by the number of units, in reverse order
    sorted_pairs = sorted(instructors_units.items(),
                          key=operator.itemgetter(1),
                          reverse=True)

    for pair in sorted_pairs:
        x_data.append(pair[0])
        y_data.append(pair[1])

        # experimental coloring of different groups
        if pair[1] >= 12:
            color_list.append('rgba(rgba(222,45,38,0.8)')
        elif pair[1] >= 6:
            color_list.append('rgba(255,128,0,0.8)')
        else:
            color_list.append('rgba(51,255,51,0.8')

    # Drawing the graph:

    trace1 = go.Bar(x=x_data,
                    y=y_data,
                    marker=dict(
                        color=color_list
                    )
                    )

    data = [trace1, ]
    layout = go.Layout(
        title='Unit load per instructor CSUN {}'.format(term),
        margin=go.Margin(
            l=60,
            r=80,
            b=80,
            t=30,
            pad=0
        ),
        legend=dict(x=.6, y=0.7),
        yaxis=dict(
            title='Units',
        ),
    )
    fig = go.Figure(data=data, layout=layout)
    url = py.plot(fig, filename='csun/overview/units_by_instructor_'+str(term), auto_open=False)
    return url, staff_units
