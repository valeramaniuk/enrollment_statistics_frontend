import logging

logger = logging.getLogger('graph_draw')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')

streamer = logging.StreamHandler()
logger.addHandler(streamer)
