import logging, os
from pathlib import Path

BASE_DIR = Path(__file__).parents[1]
filename = os.path.join(str(BASE_DIR), 'logs', 'dynamo.log')





cache_logger = logging.getLogger('cache')
cache_logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
file_handler = logging.FileHandler(filename)

streamer = logging.StreamHandler()
cache_logger.addHandler(streamer)
