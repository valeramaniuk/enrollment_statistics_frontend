from collections import OrderedDict

import datetime
from django.db import models


class CourseSpring2017(models.Model):
    #  id field will be created automatically, but it
    #  not really informative
    #  we will enforce uniqueness in two fields, since
    #  as I understand Django cannot into composite keys

    course_subject = models.CharField(max_length=20)  # COMP
    course_level = models.CharField(max_length=10)  # 101/L
    course_title = models.CharField(max_length=200)  # Computer literacy and such
    course_type = models.CharField(max_length=20)  # Lecture
    course_units = models.FloatField()

    class Meta:
        unique_together = ('course_subject', 'course_level')

    @classmethod
    def all_courses(cls):
        '''
            Returns a dictionary {<subject>: [level 1, level 2, ....]}
            The levels are sorted
        '''
        all_levels_of_subjects = OrderedDict()
        all_courses = cls.objects.all()
        for course in all_courses:

            if not course.course_subject in all_levels_of_subjects:
                all_levels_of_subjects[course.course_subject] = []

            all_levels_of_subjects[course.course_subject].append(course.course_level)

        for k, v in all_levels_of_subjects.items():
            all_levels_of_subjects[k] = sorted(v)

        return all_levels_of_subjects

    @classmethod
    def distinct_subjects(cls):
        return CourseSpring2017.objects.distinct('course_subject').values_list('course_subject', flat=True)\
                .order_by('course_subject')

    def __str__(self):
        return "{}-{}".format(self.course_subject, self.course_level)


class CourseFall2017(models.Model):
    #  id field will be created automatically, but it
    #  not really informative
    #  we will enforce uniqueness in two fields, since
    #  as I understand Django cannot into composite keys

    course_subject = models.CharField(max_length=20)  # COMP
    course_level = models.CharField(max_length=10)  # 101/L
    course_title = models.CharField(max_length=200)  # Computer literacy and such
    course_type = models.CharField(max_length=20)  # Lecture
    course_units = models.FloatField()

    class Meta:
        unique_together = ('course_subject', 'course_level')

    @classmethod
    def all_courses(cls):
        '''
            Returns a dictionary {<subject>: [level 1, level 2, ....]}
            The levels are sorted
        '''
        all_levels_of_subjects = OrderedDict()
        all_courses = cls.objects.all()
        for course in all_courses:

            if not course.course_subject in all_levels_of_subjects:
                all_levels_of_subjects[course.course_subject] = []

            all_levels_of_subjects[course.course_subject].append(course.course_level)

        for k, v in all_levels_of_subjects.items():
            all_levels_of_subjects[k] = sorted(v)

        return all_levels_of_subjects

    @classmethod
    def distinct_subjects(cls):
        return CourseSpring2017.objects.distinct('course_subject').values_list('course_subject', flat=True)\
                .order_by('course_subject')

    def __str__(self):
        return "{}-{}".format(self.course_subject, self.course_level)


class SectionSpring2017(models.Model):

    course = models.ForeignKey(CourseSpring2017, related_name="section_spring2017")  # <Course object>
    class_number = models.CharField(max_length=10, primary_key=True)  # 543563
    section_number = models.CharField(max_length=10)  # 1 or 2 or 3
    section_max_enrollment = models.IntegerField(default=0)
    publication_time = models.DateTimeField()

    def __str__(self):
        return "{} ({} {})".format(self.class_number,
                                   self.course.course_subject,
                                   self.course.course_level,
                                  )


class SectionFall2017(models.Model):

    course = models.ForeignKey(CourseFall2017, related_name="section_fall2017")  # <Course object>
    class_number = models.CharField(max_length=10, primary_key=True)  # 543563
    section_number = models.CharField(max_length=10)  # 1 or 2 or 3
    section_max_enrollment = models.IntegerField(default=0)
    publication_time = models.DateTimeField()

    def __str__(self):
        return "{} ({} {})".format(self.class_number,
                                   self.course.course_subject,
                                   self.course.course_level,
                                  )


class EnrollmentSpring2017(models.Model):
    class_number = models.ForeignKey(SectionSpring2017, related_name="enrollment")
    publication_time = models.DateTimeField()
    open_seats = models.IntegerField()

    class Meta:
        unique_together = ('class_number', 'publication_time')

    @classmethod
    def latest_pub_date(cls):
        return cls.objects.latest('publication_time').publication_time

    @classmethod
    def all_pub_times(cls, time_cutoff=None):
        if time_cutoff:
            all_times = cls.objects.filter(publication_time__lt=time_cutoff)\
                .distinct('publication_time')\
                .order_by('publication_time')\
                .values_list('publication_time', flat=True)
        else:
            all_times = cls.objects.distinct('publication_time')\
                .order_by('publication_time')\
                .values_list('publication_time', flat=True)
        return all_times

    def __str__(self):
        return "{} {}".format(self.class_number,
                              self.publication_time,
                             )


class EnrollmentFall2017(models.Model):
    class_number = models.ForeignKey(SectionFall2017, related_name="enrollment")
    publication_time = models.DateTimeField()
    open_seats = models.IntegerField()

    class Meta:
        unique_together = ('class_number', 'publication_time')

    @classmethod
    def latest_pub_date(cls):
        return cls.objects.latest('publication_time').publication_time

    @classmethod
    def all_pub_times(cls, time_cutoff=None):
        if time_cutoff:
            all_times = cls.objects.filter(publication_time__lt=time_cutoff)\
                .distinct('publication_time')\
                .order_by('publication_time')\
                .values_list('publication_time', flat=True)
        else:
            all_times = cls.objects.distinct('publication_time')\
                .order_by('publication_time')\
                .values_list('publication_time', flat=True)
        return all_times

    def __str__(self):
        return "{} {}".format(self.class_number,
                              self.publication_time,
                             )


class SectionScheduleSpring2017(models.Model):
    # one section will have several schedules in sense of "sessions"
    #   for example if the section goes from 1200-1300 and then 1330-1430
    #   then two "schedules" are needed
    id = models.AutoField(primary_key=True)
    class_number = models.ForeignKey(SectionSpring2017, related_name="section_schedule",)  # <Section object>
    room = models.CharField(max_length=50, blank=True)
    instructor = models.CharField(max_length=50, null=True)  # just a name as a string, no specifics
    days = models.CharField(max_length=10, null=True)  # "MTWHFS"
    time_start = models.TimeField(blank=True, null=True)
    time_end = models.TimeField(blank=True, null=True)
    date_start = models.DateField(blank=True, null=True)
    date_end = models.DateField(blank=True, null=True)
    publication_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        unique_together = ('class_number', 'time_start', 'time_end')

    def __str__(self):
        return "{} {} {}-{}".format(self.instructor,
                              self.days,
                              self.time_start,
                              self.time_end,
                              )
    @classmethod
    def distinct_insctructors(cls):
        instructors = cls.objects.distinct('instructor').values_list('instructor', flat=True)
        return instructors


class SectionScheduleFall2017(models.Model):
    # one section will have several schedules in sense of "sessions"
    #   for example if the section goes from 1200-1300 and then 1330-1430
    #   then two "schedules" are needed
    id=models.AutoField(primary_key=True)
    class_number = models.ForeignKey(SectionFall2017, related_name="section_schedule",)  # <Section object>
    room = models.CharField(max_length=50, blank=True)
    instructor = models.CharField(max_length=50, null=True)  # just a name as a string, no specifics
    days = models.CharField(max_length=10, null=True)  # "MTWHFS"
    time_start = models.TimeField(blank=True, null=True)
    time_end = models.TimeField(blank=True, null=True)
    date_start = models.DateField(blank=True, null=True)
    date_end = models.DateField(blank=True, null=True)
    publication_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        unique_together = ('class_number', 'time_start', 'time_end')

    def __str__(self):
        return "{} {} {}-{}".format(self.instructor,
                              self.days,
                              self.time_start,
                              self.time_end,
                              )
    @classmethod
    def distinct_insctructors(cls):
        instructors = cls.objects.distinct('instructor').values_list('instructor', flat=True)
        return instructors