# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-05-09 02:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('display_stats', '0015_course_term'),
    ]

    operations = [
        migrations.AddField(
            model_name='section',
            name='term',
            field=models.CharField(default='spring2017', max_length=20),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='section',
            unique_together=set([('class_number', 'term')]),
        ),
    ]
