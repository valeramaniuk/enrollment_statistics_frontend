# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-06 12:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('display_stats', '0006_auto_20170406_1200'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sectionschedule',
            name='days',
            field=models.CharField(max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='sectionschedule',
            name='instructor',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
