# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-05-09 02:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('display_stats', '0014_auto_20170414_1624'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='term',
            field=models.CharField(default='spring2017', max_length=20),
            preserve_default=False,
        ),
    ]
