import operator

from django.shortcuts import redirect
from django.http import HttpResponse
from django.shortcuts import render
from django.db.models import Sum
from .graphs.draw_graphs import \
                        draw_open_seats_all_subjects, \
                        draw_detailed_course_graph, \
                        draw_detailed_section_graph, \
                        draw_seats_by_subject_overview, \
                        draw_wasted_seats_by_section, \
                        draw_instructors_units
from .models import CourseSpring2017,\
                    EnrollmentSpring2017,\
                    EnrollmentFall2017,\
                    SectionSpring2017,\
                    SectionScheduleSpring2017,\
                    SectionScheduleFall2017,\
                    SectionFall2017,\
                    CourseFall2017
from dynamo import cache
from .logger import cache_logger

STATS_CACHE_ENABLED = True
GRAPHS_CACHE_ENABLED = False


term_models = {
                    'spring2017': {
                                    'section_model': SectionSpring2017,
                                    'course_model': CourseSpring2017,
                                    'section_schedule_model': SectionScheduleSpring2017,
                                    'enrollment_model': EnrollmentSpring2017,
                                  },
                    'fall2017':  {

                                    'section_model': SectionFall2017,
                                    'course_model': CourseFall2017,
                                    'section_schedule_model': SectionScheduleFall2017,
                                    'enrollment_model': EnrollmentFall2017,
                                 }
            }

def show_overview(request):
    '''
    Contains top-level aggregated data such as:
        total number of open seats
        total number of distinct courses offered
        total number of sections offered
        graph to reflect enrollment dynamic for the semester
        graph to reflect number of seats by the subject
    '''



    # TODO: the need to factor out badly!!!!

    term_data = {
                    'spring2017': {
                                    'total_seats': 0,
                                    'number_of_courses': 0,
                                    'number_of_sections': 0,
                                    'number_of_instructors': 0,
                                    'dynamic_of_enrollment_embed': '',
                                    'seats_by_subject_overview_embed': '',
                                    'wasted_seats_by_section_overview_embed': '',
                                    'draw_instructors_units_embed': '',
                                    'staff_units': '',
                                  },
                    'fall2017':  {
                                    'total_seats': 0,
                                    'number_of_courses': 0,
                                    'number_of_sections': 0,
                                    'number_of_instructors': 0,
                                    'dynamic_of_enrollment_embed': '',
                                    'seats_by_subject_overview_embed': '',
                                    'wasted_seats_by_section_overview_embed': '',
                                    'draw_instructors_units_embed': '',
                                    'staff_units': '',
                                  },
    }


    for term in term_data:

        # Total number of seats at the beginning of the semester <term>
        is_cache = cache.check_if_cache_exists('total_seats_' + str(term))
        if (is_cache is not None)&  STATS_CACHE_ENABLED:
            cache_logger.debug('Total seats {} HIT'.format(term))
            term_data[term]['total_seats'] = is_cache
        else:
            cache_logger.debug('Total seats {} MISS'.format(term))
            term_data[term]['total_seats'] = (term_models[term]['section_model']).objects.filter()\
                                .aggregate(Sum('section_max_enrollment'))['section_max_enrollment__sum']
            cache.create_cache('total_seats_' + str(term),  term_data[term]['total_seats'])

        # Total number of courses  throughout  the Spring 2017 semester
        is_cache = cache.check_if_cache_exists('number_of_courses_'+term)
        if (is_cache is not None) & STATS_CACHE_ENABLED:
            cache_logger.debug('Number of courses {} HIT'.format(term))
            term_data[term]['number_of_courses'] = is_cache
        else:
            cache_logger.debug('Number of courses {} MISS'.format(term))
            term_data[term]['number_of_courses'] = (term_models[term]['course_model']).objects.count()
            cache.create_cache('number_of_courses_'+term, term_data[term]['number_of_courses'])

        # Total number of sections  throughout  the semester
        is_cache = cache.check_if_cache_exists('number_of_sections_'+str(term))
        if (is_cache is not None) & STATS_CACHE_ENABLED:
            cache_logger.debug('Number of sections {} HIT'.format(term))
            term_data[term]['number_of_sections'] = is_cache
        else:
            cache_logger.debug('Number of sections {} MISS'.format(term))
            term_data[term]['number_of_sections'] = (term_models[term]['section_model']).objects.count()
            cache.create_cache('number_of_sections_'+str(term), term_data[term]['number_of_sections'])


        # Total number of instructors
        is_cache = cache.check_if_cache_exists('number_of_instructors_'+str(term))
        if (is_cache is not None) & STATS_CACHE_ENABLED:
            cache_logger.debug('Number of instructors HIT'.format(term))
            term_data[term]['number_of_instructors'] = is_cache
        else:
            cache_logger.debug('Number of instructors %s MISS'.format(term))
            term_data[term]['number_of_instructors'] = (term_models[term]['section_schedule_model']).objects\
                                                            .distinct('instructor')\
                                                            .count()
            cache.create_cache('number_of_instructors_'+str(term), term_data[term]['number_of_instructors'])

# GRAPHS

        # # Graph of enrollment dynamics
        # is_cache = cache.check_if_cache_exists('dynamics_of_enrollment_'+str(term))
        # if (is_cache is not None) &  GRAPHS_CACHE_ENABLED:
        #     cache_logger.debug('dynamics_of_enrollment {} HIT'.format(term))
        #     term_data[term]['dynamic_of_enrollment_embed'] = is_cache
        # else:
        #     cache_logger.debug('dynamics_of_enrollment {} MISS'.format(term))
        #     term_data[term]['dynamic_of_enrollment_embed'] = draw_open_seats_all_subjects(term_models, term)
        #     cache.create_cache('dynamics_of_enrollment_'+str(term), term_data[term]['dynamic_of_enrollment_embed'])

        # # Graph of open seats for subjects. Bar
        # is_cache = cache.check_if_cache_exists('seats_by_subject_overview_'+str(term))
        # if (is_cache is not None) & GRAPHS_CACHE_ENABLED:
        #     cache_logger.debug('seats_by_subject_overview {} HIT'.format(term))
        #     term_data[term]['seats_by_subject_overview_embed'] = is_cache
        # else:
        #     cache_logger.debug('seats_by_subject_overview {} MISS'.format(term))
        #     term_data[term]['seats_by_subject_overview_embed'] = draw_seats_by_subject_overview(term_models, term)
        #     cache.create_cache('seats_by_subject_overview_'+str(term), term_data[term]['seats_by_subject_overview_embed'])

        # # Graph of wasted seats by the final date, by section
        # is_cache = cache.check_if_cache_exists('wasted_seats_by_section_overview_'+str(term))
        # if (is_cache is not None) & GRAPHS_CACHE_ENABLED:
        #     cache_logger.debug('wasted_seats_by_section_overview {} HIT'.format(term))
        #     term_data[term]['wasted_seats_by_section_overview_embed'] = is_cache
        # else:
        #     cache_logger.debug('wasted_seats_by_section_overview {} MISS'.format(term))
        #     term_data[term]['wasted_seats_by_section_overview_embed'] = draw_wasted_seats_by_section(term_models, term)
        #     cache.create_cache('wasted_seats_by_section_overview_'+str(term), term_data[term]['wasted_seats_by_section_overview_embed'])

        # Graph of unit load per instructor
        is_cache = cache.check_if_cache_exists('unit_load_by_instructor_overview_'+str(term))
        if (is_cache is not None) & GRAPHS_CACHE_ENABLED:
            cache_logger.debug('unit_load_by_instructor_overview {} HIT'.format(term))
            term_data[term]['draw_instructors_units_embed'] = is_cache
            term_data[term]['staff_units'] = int(cache.check_if_cache_exists('unit_load_by_staff_units_'+str(term)))
        else:
            cache_logger.debug('unit_load_by_instructor_overview {} MISS'.format(term))

            term_data[term]['draw_instructors_units_embed'],\
            term_data[term]['staff_units'] = draw_instructors_units(term_models, term)

            cache.create_cache('unit_load_by_instructor_overview_'+str(term), term_data[term]['draw_instructors_units_embed'])
            cache.create_cache('unit_load_by_staff_units_'+str(term), int(term_data[term]['staff_units']))


    return render(request,
                  'display_stats/overview.html',
                  {
                      'term_data': term_data,
                  }
                  )


def show_list_of_courses(request, term = 'fall2017', course_subject=None, course_level=None):
    ''' Shows the list of all courses ever appeared in the schedule.
        If the course subject or course subject and course level
        are specified - also shows the graph with the aggregated data
        for the subject or specific course + graphs for all sections
        for such course '''


    # get a dictionary {<subject>: [level 1, level 2, ....]}
    all_levels_of_all_subjects = (term_models[term]['course_model']).all_courses()
    # get a list of all possible subjects
    list_of_subjects = (term_models[term]['course_model']).distinct_subjects()

    # if course IS specified
    if course_subject:
        # initialized so IDE won't whine, probably unnecessary
        course = None
        course_subject = course_subject.replace('%20', ' ')

        # and the level is also specified
        # draw the graph aggregated graph for course+level
        # draw graphs for all sections which belong to this course
        # pass section objects to the template
        # pass list of all courses to the template
        if course_level:

            try:
                course_embed = draw_detailed_course_graph(course_subject, course_level)
            except Exception as e:
                course_embed = None
                # TODO: add logging

            # TODO: make sure that the graph drawing func does that replacement and then delete it
            # embed = url.replace('https:', '') + '.embed'

            course = (term_models[term]['course_model']).objects.get(
                course_subject=course_subject,
                course_level=course_level
            )
            all_sections = (term_models[term]['section_model']).objects.filter(course=course)
            list_of_section_embeds = draw_detailed_section_graph(all_sections)

            # to pass urls to graphs and section info together to the template
            list_of_section_embeds_and_section_info = zip(list_of_section_embeds, all_sections)

            return render(request, 'display_stats/courses.html',
                          {'course_embed': course_embed,
                           'course': course,
                           'list_of_section_embeds_and_section_info': list_of_section_embeds_and_section_info,
                           'all_levels': all_levels_of_all_subjects,
                           'list_of_subjects': list_of_subjects})
        else:
            # if only course subject is specified
            # draw the aggregated graph for the subject
            # pass list of all courses to the template
            try:
                course_embed = draw_detailed_course_graph(subject=course_subject)
            except Exception as e:
                course_embed = None
                # TODO: add logging
            return render(request, 'display_stats/courses.html',
                          {'course_embed': course_embed,
                           'course_subject': course_subject,
                           'all_levels': all_levels_of_all_subjects,
                           'list_of_subjects': list_of_subjects})



    # if course is not specified - shows only the list of all courses
    else:
        return render(request, 'display_stats/courses.html',
                      {'all_levels': all_levels_of_all_subjects,
                       'list_of_subjects': list_of_subjects})


def show_instructors(request, instructor=None):
    from collections import OrderedDict
    course_to_instructor = dict()
    all_sections = None
    sections_with_graphs = None

    if instructor:
        instructor = instructor.replace('%20', ' ')
        schedules = SectionScheduleSpring2017.objects.filter(instructor=instructor)
        all_sections = SectionSpring2017.objects.filter(section_schedule__in=schedules)
        list_of_urls = draw_detailed_section_graph(all_sections)
        sections_with_graphs = zip(all_sections, list_of_urls)

    # check if cache exists
    # if it does - it returns the dictionary
    #  { <course> : [<instructor 1>, <instructor 2>] }
    # and we transform it to the Ordered dictionary sorted by the course
    is_cache = cache.check_if_cache_exists('instructors')
    if is_cache:
        cache_logger.debug("HIT instructor list cache")
        temp_dict = dict()
        for k, v in is_cache.items():
            v = list(v)
            v.sort()
            temp_dict[k] = v
        course_to_instructor = OrderedDict(sorted(temp_dict.items()))

        # if cache doesn't exists
    else:
        cache_logger.debug("MISS instructor list cache")
        all_subjects = CourseSpring2017.distinct_subjects()

        # go through each subject one by one
        # get all sections for the subject
        # get all schedules for every section
        # gets all instructor from these schedules
        #
        # The problem here is that it shows ALL instructors ever mentioned in the
        # schedule. If an instructor was in the schedule but ended up never teaching any course
        # he/she will appear anyways
        #
        for subject in all_subjects:
            sections_for_course = SectionSpring2017.objects.filter(course__course_subject=subject)
            all_instructors_for_subject = SectionScheduleSpring2017.objects.filter(
                class_number__in=sections_for_course). \
                values_list('instructor', flat=True)

            # get rid of duplicates
            instructors_set = set(all_instructors_for_subject)
            instructors_set = list(instructors_set)
            instructors_set.sort()
            cache_logger.debug('There are {} instructors'.format(len(instructors_set)))
            cache_logger.debug('Adding instructors to {}, there are {} of them'.format(subject, len(instructors_set)))
            course_to_instructor[subject] = instructors_set

        cache.create_cache('instructors', course_to_instructor)
    course_to_instructor = OrderedDict(sorted(course_to_instructor.items()))
    return render(request, 'display_stats/instructors.html',
                  {
                      'course_to_instructor': course_to_instructor,
                      'all_sections': all_sections,
                      'sections_with_graphs': sections_with_graphs,
                      'instructor': instructor
                  }
                  )


def show_about(request):
    return render(request, 'display_stats/about.html')


def show_all_pdfs_in_bucket(request, term):
    import boto3
    BUCKET_FALL2017 = 'csunpdffall2017'
    BUCKET_SPRING2017 = 'csunpdfspring2017'
    list_of_obj = []
    s3 = boto3.resource('s3')
    if term == 'spring2017':
        csun_bucket = s3.Bucket(BUCKET_SPRING2017)
        BUCKET = BUCKET_SPRING2017
    else:
        csun_bucket = s3.Bucket(BUCKET_FALL2017)
        BUCKET = BUCKET_FALL2017

    all_objects = csun_bucket.objects.all()
    count = sum(1 for _ in all_objects)
    for obj in all_objects:
        url = 'https://s3-us-west-2.amazonaws.com/{}/'.format(BUCKET) + obj.key
        list_of_obj.append((obj.key, obj.last_modified, url))

    return render(request, 'display_stats/raw_data.html',
                  {
                      'list_of_obj': list_of_obj,
                      'count': count
                  }
                  )


def single_section(request, class_number):
    section = SectionSpring2017.objects.get(class_number=class_number)
    schedule = SectionScheduleSpring2017.objects.filter(class_number=class_number)
    enrollments = EnrollmentSpring2017.objects.filter(class_number=class_number).order_by('publication_time')

    x_data = []
    y_data = []
    for enr in enrollments:
        x_data.append(enr.publication_time)
        y_data.append(enr.open_seats)

    emb = draw_detailed_section_graph([section, ])

    context = {'section': section,
               'schedule': schedule,
               'enrollments': enrollments,
               'emb': emb[0]}

    return render(request, 'display_stats/single_section.html', context)


def debug(request):
    draw_instructors_units_embed = draw_instructors_units()

    return render(request, 'display_stats/debug.html', {'draw_instructors_units_embed': draw_instructors_units_embed})
