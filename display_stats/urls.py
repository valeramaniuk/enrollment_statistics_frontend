from django.conf.urls import url
from django.http import HttpResponse


from . import views

urlpatterns = [
    url(r'^robots.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: /", mimetype="text/plain")),
    url(r'^$', views.show_overview, name='overview'),
    url(r'^about$', views.show_about, name='about'),

    url(r'^instructors/$', views.show_instructors, name='instructors'),
    url(r'^instructor_detailed/(?P<instructor>[-,\w ]+)', views.show_instructors, name='instructors'),


    url(r'^courses/$', views.show_list_of_courses, name='courses_list'),
    url(r'^courses/(?P<course_subject>[-\w ]+)/$',
        views.show_list_of_courses, name="courses_subject_only"),
    url(r'^courses/(?P<course_subject>[-\w ]+)/(?P<course_level>[-\w]+)/$',
        views.show_list_of_courses, name="courses_subject_level"),

    url(r'^raw_data/$', views.show_all_pdfs_in_bucket, name='raw_data'),
    url(r'^raw_data/(?P<term>[\w]+)/$', views.show_all_pdfs_in_bucket, name='raw_data_term'),

    url(r'^debug$', views.debug, name='debug'),
    url(r'^single_section/(?P<class_number>[\d]+)', views.single_section, name='single_section'),
    ]