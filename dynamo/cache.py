import boto3
from .dynamo_logger import logger


dynamodb = boto3.resource('dynamodb', region_name='us-west-2')
cache_table = dynamodb.Table('cache_csun')

_CACHE_ENABLED = True

def retrieve_or_cache(meta, query):

    cache = check_if_cache_exists(meta)
    if cache and _CACHE_ENABLED:
        return cache
    else:
        # I'm going to call query twice, not sure how many times
        # it will be called
        # just to be safe I reassign its value to a variable
        new_query = query
        create_cache(meta, new_query)
        return new_query


def check_if_cache_exists(meta):

    response = cache_table.get_item(
        Key={
            'keys': meta
        }
    )
    try:
        if response['Item']['cache'] and _CACHE_ENABLED:
            return response['Item']['cache']
        else:
            return False
    except KeyError:
        return False


def create_cache(meta, to_cache):
    if to_cache:
        cache_table.put_item(
            Item={
                'keys': meta,
                'cache': to_cache
            },
        )
        return True
    else:
        return False


def flush_graphs_cache():

    last_key = True
    while last_key:
        scan = cache_table.scan(
            AttributesToGet=[
                'keys',
            ]
        )
        logger.info('Scan results:\n', scan)
        last_key = 'LastEvaluatedKey' in scan
        for item in scan['Items']:
            response =  cache_table.delete_item(
                    Key = {
                        'keys': item['keys'],
                    }
            )
            logger.info('Deleting...{}'.format(response))