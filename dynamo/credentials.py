import boto3
from .dynamo_logger import logger

dynamodb = boto3.resource('dynamodb', region_name='us-west-2')
configuration_table = dynamodb.Table('configuration')

def get_plotly_credentials():

    response = configuration_table.get_item(
        Key={
            'keys': 'plotly_signin'
        }
    )
    try:
        if response['Item']['values']:
            logger.info('Getting credentials for plot.ly SUCCESSFUL')
            return response['Item']['values']
        else:
            logger.critical('Getting credentials for plot.ly FAILED')
            return False
    except KeyError:
        logger.critical('Getting credentials for plot.ly FAILED')
        return False

def get_db_password(db_name):
    logger.info('Getting the password for {}'.format(db_name))
    response = configuration_table.get_item(
        Key={
            'keys': db_name+"_conf"
        }
    )
    try:
        if response['Item']['values']:
            logger.info('Getting the password for {} SUCCESSFUL'.format(db_name))
            return response['Item']['values']
        else:
            logger.critical('Getting the password for {} FAILED'.format(db_name))
            return False
    except KeyError:
        logger.critical('Getting the password for {} FAILED'.format(db_name))
        return False